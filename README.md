
<h1 style="text-align: center; color: hotpink; -webkit-animation: rainbow 5s infinite; -moz-animation: rainbow 5s infinite; -o-animation: rainbow 5s infinite; animation: rainbow 5s infinite;">ChatGPT Java Api</h1>
 

![stable](https://img.shields.io/badge/stability-stable-brightgreen.svg)
[![Maven Central](https://img.shields.io/maven-central/v/com.github.plexpt/chatgpt)](https://maven-badges.herokuapp.com/maven-central/com.github.plexpt/chatgpt)  

[使用gpt-3.5-turbo-16k做的聊天demo](http://hoppinzq.com/chat/index.html).  

[chatgpt接口文档离线版](https://hoppinzq.com/manager/chatgptAPI.html).  


[聊天demo前端源代码](https://gitee.com/hoppin/chatgpt-front).  
[chatgpt后台代理源代码](https://gitee.com/hoppin/java-service-management-system/tree/master/hoppinzq-chatgpt).  
  

[我的博客网站](http://hoppin.cn/).  
[我的视频网站](https://hoppinzq.com/video/index.html).


由于openai开放了chatgpt接口，已经不提供支持，直接调用api就行了，具体案例和如何调用请看上面演示